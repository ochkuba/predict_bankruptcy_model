import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#file loading

df = pd.read_csv('dataset.csv', sep=';', index_col=0)
df.head()

#change 'SECTOR' strings into numbers

df['SECTOR'].unique()
sec_dict = {'PUB': 1, 'IT' : 2, 'CAFE':3, 'RESTAURANT':4}
df['SECTOR'] = df['SECTOR'].map(sec_dict)

df['PUB'] = (df['SECTOR']==1).astype('int')
df['IT'] = (df['SECTOR']==2).astype('int')
df['CAFE'] = (df['SECTOR']==3).astype('int')
df['RESTAURANT'] = (df['SECTOR']==4).astype('int')
df.drop('SECTOR',axis = 1)


data = df.drop(['FLAG'],axis = 1)

from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split, cross_val_score, KFold
from sklearn.linear_model import LogisticRegression 
from sklearn.preprocessing import RobustScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix, precision_recall_curve
from sklearn.pipeline import make_pipeline

# assignment of test and training data

X = data
robust_scaler = RobustScaler()
X = robust_scaler.fit_transform(X)
y = df['FLAG']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, stratify=y)

# KFold test Logistic Regression

features = X_train
target = y_tain
standarizer = RobustScaler()
logit = LogisticRegression()
pipeline = make_pipeline(standarizer,logit)
kf = KFold(n_splits=10, shuffle=True, random_state=1)

cv_results = cross_val_score(pipeline,features,target,cv=kf,scoring = 'accuracy',n_jobs=-1)
cv_results.mean()

# KFold test Decision Tree

features = X_train
target = y_train
standarizer = RobustScaler()
logit = DecisionTreeClassifier()
pipeline = make_pipeline(standarizer,logit)
kf = KFold(n_splits=10, shuffle=True, random_state=1)

cv_results = cross_val_score(pipeline,features,target,cv=kf,scoring = 'accuracy',n_jobs=-1)
cv_results.mean()

#preparing data frame for results 

metrics = pd.DataFrame(index=['accuracy','precision','recall'],
                      columns=['LogisticReg','ClassTree'])
                      

#logistic regression

logistic_regresion = LogisticRegression(n_jobs=-1)
logistic_regresion.fit(X_train, y_train)

y_pred_test = logistic_regresion.predict(X_test)
metrics.loc['accuracy','LogisticReg']=accuracy_score(y_pred=y_pred_test, y_true=y_test)
metrics.loc['precision','LogisticReg']=precision_score(y_pred=y_pred_test, y_true=y_test)
metrics.loc['recall','LogisticReg']=recall_score(y_pred=y_pred_test, y_true=y_test)


#classification tree
class_tree = DecisionTreeClassifier(min_samples_split=1000, min_samples_leaf=100)
class_tree.fit(X_train,y_train)

y_pred_test = class_tree.predict(X_test)
metrics.loc['accuracy','ClassTree']=accuracy_score(y_pred=y_pred_test, y_true=y_test)
metrics.loc['precision','ClassTree']=precision_score(y_pred=y_pred_test, y_true=y_test)
metrics.loc['recall','ClassTree']=recall_score(y_pred=y_pred_test, y_true=y_test)

# print results accuracy, precison, recall

100*metrics

# preparing dataframe for F1 value

metric = pd.DataFrame(index=['F1'],
                      columns=['LogisticReg','ClassTree'])
                      
s1 = cross_val_score(logistic_regresion, X, y, scoring="f1")
metric.loc['F1','LogisticReg']= s1.mean()

s2 = cross_val_score(class_tree, X, y, scoring="f1")
metric.loc['F1','ClassTree']= s2.mean()

metric *100
